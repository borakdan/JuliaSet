#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <stdint.h>
#include <stdbool.h>
#include <complex.h>
#include <inttypes.h>
#include <math.h>

#include "font_types.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "mzapo_parlcd.h"

// Start while loop
bool active = true;

pthread_t thread_compute, thread_render;

//  Menu
int number_of_rows = 3;
int brightness = 9;
int colorMode = 0;
int version = 0;
int last_version = 0;
int maxNumberOfColorMode = 9;
int maxNumberOfVersion = 7;

//	Initial parameters
int iterationsJulia = 150;
double cRe = -0.4;
double cIm = 0.6;
int indexC = 100;
double offsetX = -240;
double offsetY = -160;

uint16_t display_array[480][320];


double x = -200;
double y = -200;
double last_x = -240;
double last_y = -160;
int last_C = 100;
double last_cRe = -0.4;
double last_cIm = 0.6;
double temp_cRe = -0.4;
double temp_cIm = 0.6;
double cReRand = 1;
double cImRand = 1;
int redrawX = 0;
int redrawY = 0;
int dispArrayAvailable = 0;
int presentation_active = 0;
int current_display_style = 1;





//	Compute thread  //

uint16_t getJuliaColor(int thisIteration) {
	unsigned char r, g, b;
	double red = 255;
	double green = 255;
	double blue = 255;
    unsigned short rgb;
	
	double t = (double) thisIteration / (double) iterationsJulia;
	double thisBrightness = (double) brightness * 10.0 / 9.0;
	
	if (thisIteration == -1) {
		//return 0x001F;
		return 0;
	}
	
	if (colorMode == 0) {
		red = 9*(1-t)*t*t*t * 255;
		green = 15*(1-t)*(1-t)*t*t * 255;
		blue = 8.5*(1-t)*(1-t)*(1-t)*t * 255;
	} else if (colorMode == 1) {
		red = 9*(1-t)*t*t*t * 255 * thisBrightness;
		green = 15*(1-t)*(1-t)*t*t * 255 * thisBrightness;
		blue = 8.5*(1-t)*(1-t)*(1-t)*t * 255 * thisBrightness;
	} else if (colorMode == 2) {
		red = 4.5*(1-t)*t*t*t * 255;
		green = 9*(1-t)*(1-t)*t*t * 255;
		blue = 12.5*(1-t)*(1-t)*(1-t)*t * 255;
	} else if (colorMode == 3) {
		red = 4.5*(1-t)*t*t*t * 255 * thisBrightness;
		green = 9*(1-t)*(1-t)*t*t * 255 * thisBrightness;
		blue = 12.5*(1-t)*(1-t)*(1-t)*t * 255 * thisBrightness;
	} else if (colorMode == 4) {
		red = 8.5*(1-t)*t*t*t * 255;
		green = 6*(1-t)*(1-t)*(1-t)*t * 255;
		blue = 2.5*(1-t)*(1-t)*t*t * 255;
	} else if (colorMode == 5) {
		red = 8.5*(1-t)*t*t*t * 255 * thisBrightness;
		green = 6*(1-t)*(1-t)*(1-t)*t * 255 * thisBrightness;
		blue = 2.5*(1-t)*(1-t)*t*t * 255 * thisBrightness;
	} else if (colorMode == 6) {
		red = 12*(1-t)*(1-t)*(1-t)*(1-t) * 255;
		green = 4*(1-t)*(1-t)*(1-t)*t * 255;
		blue = 1.5*(1-t)*(1-t)*t*t * 255;
	} else if (colorMode == 7) {
		red = 12*(1-t)*(1-t)*(1-t)*(1-t) * 255 * thisBrightness;
		green = 4*(1-t)*(1-t)*(1-t)*t * 255 * thisBrightness;
		blue = 1.5*(1-t)*(1-t)*t*t * 255 * thisBrightness;
	} else if (colorMode == 8) {
		red = 15*(1-t)*t*t*t * 255;
		green = 6*(1-t)*(1-t)*(1-t)*t * 255;
		blue = 4*t*t*t*t * 255;
	} else if (colorMode == 9) {
		red = 15*(1-t)*t*t*t * 255 * thisBrightness;
		green = 6*(1-t)*(1-t)*(1-t)*t * 255 * thisBrightness;
		blue = 4*t*t*t*t * 255 * thisBrightness;
	}
	
	if (red > 255) red = 255;
	if (green > 255) green = 255;
	if (blue > 255) blue = 255;
	if (red < 0) red = 0;
	if (green < 0) green = 0;
	if (blue < 0) blue = 0;
    r = (char) red >> 3;
    g = (char) green >> 2;
    b = (char) blue >> 3;
    rgb = (r << 11) | (g << 5) | b;
	//return 0xc350;
	return rgb;
}

int JuliaCompute(double real, double imaginary) {
	int i = 0;
	double newRe, newIm, oldRe, oldIm;
	newRe = real;
	newIm = imaginary;
	if ((newRe * newRe + newIm * newIm) > 4) {
		return 0;
	}
	for (i = 1; i < iterationsJulia; i++) {
		oldRe = newRe;
		oldIm = newIm;
		newRe = oldRe * oldRe - oldIm * oldIm + temp_cRe;
		newIm = 2 * oldRe * oldIm + temp_cIm;

		//if the point is outside the circle with radius 2: stop
		if ((newRe * newRe + newIm * newIm) > 4) return i;
	}
	return -1;
}

// Compute thread
void *Compute(void* args) {
	int counter = 0;
	dispArrayAvailable = 0;
	for(int y = 0; y < 320; y++) {
		for(int x = 0; x < 480; x++) {
			double real_this = 0.005 * (x + offsetX);
			double imaginary_this = 0.005 * (y + offsetY);
			if (JuliaCompute(real_this, imaginary_this) <= 5) counter++;
			display_array[x][y] = getJuliaColor(JuliaCompute(real_this, imaginary_this));
		}
	}
	dispArrayAvailable = 1;

	while(active) {
		if (last_C != indexC || last_cRe != cRe || last_cIm != cIm || last_version != version) {
			if (version == 0) {
				temp_cRe = cRe * (double) indexC / 100.0;
				temp_cIm = cIm * (double) indexC / 100.0 * (double) indexC / 100.0;
			} else if (version == 1) {
				temp_cRe = cRe * (double) indexC / 100.0;
				temp_cIm = cIm * (double) indexC / 100.0;
			} else if (version == 2) {
				temp_cRe = cRe * (double) indexC / 100.0;
				temp_cIm = cIm * (double) indexC / 250.0 * (double) indexC / 100.0;
			} else if (version == 3) {
				temp_cRe = cRe * cRe * (double) indexC / 80.0;
				temp_cIm = cIm * (double) indexC / 120.0;
			} else if (version == 4) {
				temp_cRe = cRe * (double) indexC / 350.0;
				temp_cIm = cIm * cIm * (double) indexC / 750.0;
			} else if (version == 5) {
				temp_cRe = cRe * cRe * (double) indexC / 150.0;
				temp_cIm = cIm * cIm * (double) indexC / 50.0;
			} else if (version == 6) {
				temp_cRe = cRe * cRe * cRe * (double) indexC / 100.0;
				temp_cIm = cIm * (double) indexC / 150.0;
			} else if (version == 7) {
				temp_cRe = cRe * (double) indexC / 1000.0;
				temp_cIm = cIm * (double) indexC / 1000.0;
			} else {
				temp_cRe = cRe * (double) indexC / 10.0;
				temp_cIm = cIm * (double) indexC / 10.0 * (double) indexC / 10.0;
			}
			last_C = indexC;
			last_cRe = cRe;
			last_cIm = cIm;
			last_x = offsetX;
			last_y = offsetY;
			last_version = version;
			dispArrayAvailable = 0;
			for(int y = 0; y < 320; y++) {
				for(int x = 0; x < 480; x++) {
					double real_this = 0.005 * (x + offsetX);
					double imaginary_this = 0.005 * (y + offsetY);
					display_array[x][y] = getJuliaColor(JuliaCompute(real_this, imaginary_this));
				}
			}
			dispArrayAvailable = 1;
			continue;
		}
	
		if (last_x != offsetX || last_y != offsetY){
			printf("redraw X Y\n");
			last_x = offsetX;
			last_y = offsetY;
			dispArrayAvailable = 0;
			for(int y = 0; y < 320; y++) {
				for(int x = 0; x < 480; x++) {
					double real_this = 0.005 * (x + offsetX);
					double imaginary_this = 0.005 * (y + offsetY);
					display_array[x][y] = getJuliaColor(JuliaCompute(real_this, imaginary_this));
				}
			}
			dispArrayAvailable = 1;
			continue;
		}
	}
	return 0;
}








//	Rendering  //

// Print char
void printChar(char c, int row, int column, uint16_t color, uint16_t background){
    for(int i = 0; i < 16; i++)
        for(int u = 0; u < 8; u++)
            display_array[i+row*16][column*8 - u] = (font_rom8x16.bits[(int)c*16+i]>>(15-u)) & 1 ? color : background;
}

// Print string
void printfString(char* c, int row, int offset, uint16_t color, uint16_t background_color){
    int len = strlen(c);
    //char buffer[36];
    for(int i = 0; i < len; i++)
        printChar(c[i], row, offset-i, color, background_color);
}

// Redraw device
void redraw(unsigned char* parlcd_mem_base){
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for(int i = 0; i < 320; i++)
        for(int u = 0; u < 480; u++)
            parlcd_write_data(parlcd_mem_base, display_array[u][i]);
}

// Blacks out display
void redrawToBlankScreen(){
    for(int i = 0; i < 320; i++)
        for(int u = 0; u < 480; u++)
            display_array[u][i] = 0x0000;
}

// Write double in buffer
void writeDoubleInBuffer(double number3, char buffer[32]) {
	int number = (int) number3;
	number3 -= number;
	if (number3 < 0) {
		buffer[7] = '-';
		number *= -1;
		number3 *= -1;
	}
	int i = 0;
	for (i = 0; i < 7; i++) {
		buffer[8 + i] = number % 10 + '0';
		number /= 10;
		if (number == 0) break;
	}
	char temp[10];
	for (int j = 0; j <= i; j++) {
		temp[j] = buffer[8+j];
	}
	for (int j = 0; j <= i; j++) {
		buffer[8+j] = temp[i-j];
	}

	i++;
	buffer[8+i] = '.';
	number3 *= 100000000;
	number = (int) number3;
	buffer[8+i+8] = number % 10 + '0';
	number /= 10;
	buffer[8+i+7] = number % 10 + '0';
	number /= 10;
	buffer[8+i+6] = number % 10 + '0';
	number /= 10;
	buffer[8+i+5] = number % 10 + '0';
	number /= 10;
	buffer[8+i+4] = number % 10 + '0';
	number /= 10;
	buffer[8+i+3] = number % 10 + '0';
	number /= 10;
	buffer[8+i+2] = number % 10 + '0';
	number /= 10;
	buffer[8+i+1] = number % 10 + '0';
	number /= 10;
	buffer[8+i+9] = '\0';
}

// Write int in buffer
void writeIntInBuffer(int number, char buffer[32]) {
	if (number < 0) {
		buffer[7] = '-';
		number *= -1;
	}
	int i = 0;
	for (i = 0; i < 7; i++) {
		buffer[8 + i] = number % 10 + '0';
		number /= 10;
		if (number == 0) break;
	}
	char temp[10];
	for (int j = 0; j <= i; j++) {
		temp[j] = buffer[8+j];
	}
	for (int j = 0; j <= i; j++) {
		buffer[8+j] = temp[i-j];
	}
	buffer[8+i+1] = '\0';
}

// Draw menu
void drawMenu(unsigned char* parlcd_mem_base, int selected_row) {
	redrawToBlankScreen();
	char buffer[32];
	int i = 0;
	for(i = 0; i < number_of_rows; i++){
	    for(int u = 0; u < 32; u++) {
	    	buffer[u] = ' ';
		}
		
		if (i == 0 && selected_row == 1) {
			sprintf(buffer, "Bright");
			buffer[6] = ' ';
			writeIntInBuffer(brightness, buffer);
        	printfString(buffer, i, -60, 0xFFFF, 0x0000);
		}
		if (i == 0 && selected_row != 1) {
			sprintf(buffer, "Bright");
			buffer[6] = ' ';
			writeIntInBuffer(brightness, buffer);
        	printfString(buffer, i, -60, 0xc350, 0x0000);
		}
		if (i == 1 && selected_row == 2) {
			sprintf(buffer, "Color");
			buffer[5] = ' ';
			writeIntInBuffer(colorMode, buffer);
        	printfString(buffer, i, -60, 0xFFFF, 0x0000);
		}
		if (i == 1 && selected_row != 2) {
			sprintf(buffer, "Color");
			buffer[5] = ' ';
			writeIntInBuffer(colorMode, buffer);
        	printfString(buffer, i, -60, 0xc350, 0x0000);
		}
		if (i == 2 && selected_row == 3) {
			sprintf(buffer, "Version");
			buffer[7] = ' ';
			writeIntInBuffer(version, buffer);
        	printfString(buffer, i, -60, 0xFFFF, 0x0000);
		}
		if (i == 2 && selected_row != 3) {
			sprintf(buffer, "Version");
			buffer[7] = ' ';
			writeIntInBuffer(version, buffer);
        	printfString(buffer, i, -60, 0xc350, 0x0000);
		}
    }
    for(int u = 0; u < 32; u++) {
    	buffer[u] = ' ';
	}
	buffer[0] = 'X';
	buffer[1] = ':';
    writeIntInBuffer(offsetX, buffer);
    printfString(buffer, i++, -60, 0xc350, 0x0000);
    for(int u = 0; u < 32; u++) {
    	buffer[u] = ' ';
    }
    buffer[0] = 'Y';
	buffer[1] = ':';
	writeIntInBuffer(offsetY, buffer);
	printfString(buffer, i++, -60, 0xc350, 0x0000);
    for(int u = 0; u < 32; u++) {
		buffer[u] = ' ';
    }
	if (current_display_style == 3) {
		buffer[0] = 'C';
		buffer[2] = 'R';
		buffer[3] = 'e';
		buffer[4] = ':';
		writeDoubleInBuffer(cReRand, buffer);
		printfString(buffer, i++, -60, 0xc350, 0x0000);
		
		buffer[0] = 'C';
		buffer[2] = 'I';
		buffer[3] = 'm';
		buffer[4] = ':';
		writeDoubleInBuffer(cImRand, buffer);
		printfString(buffer, i++, -60, 0xc350, 0x0000);
	} else {
		buffer[0] = 'C';
		buffer[2] = 'R';
		buffer[3] = 'e';
		buffer[4] = ':';
		writeDoubleInBuffer(temp_cRe, buffer);
		printfString(buffer, i++, -60, 0xc350, 0x0000);
		
		buffer[0] = 'C';
		buffer[2] = 'I';
		buffer[3] = 'm';
		buffer[4] = ':';
		writeDoubleInBuffer(temp_cIm, buffer);
		printfString(buffer, i++, -60, 0xc350, 0x0000);
	}

    redraw(parlcd_mem_base);
}

// Draw Julia
void drawJulia(unsigned char* parlcd_mem_base){
    redraw(parlcd_mem_base);
}

// Read knobs
void read_knobs(uint8_t* k1, uint8_t* k2, uint8_t* k3, uint8_t* b1, uint8_t* b2, uint8_t* b3, unsigned char* knobs_mem_base){
    uint32_t rgb_knobs_value = *(volatile uint32_t*)(knobs_mem_base + SPILED_REG_KNOBS_8BIT_o);
    *k1 = (uint8_t)(rgb_knobs_value & 0xFF);
    *k2 = (uint8_t)((rgb_knobs_value >> 8) & 0xFF);
    *k3 = (uint8_t)((rgb_knobs_value >> 16) & 0xFF);
    *b1 = (uint8_t)((rgb_knobs_value >> 24) & 1);
    *b2 = (uint8_t)((rgb_knobs_value >> 25) & 1);
    *b3 = (uint8_t)((rgb_knobs_value >> 26) & 1);
    /* Store the read value to the register controlling individual LEDs */
    *(volatile uint32_t*)(knobs_mem_base + SPILED_REG_LED_LINE_o) = rgb_knobs_value;
}

float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

void *Rendering(void* args){
    // Map knobs
    unsigned char* knobs_mem_base = (unsigned char*)map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
    if(knobs_mem_base == NULL){
        fprintf(stderr, "Error - knobs.\n");
        return NULL;
    }
    // Map display
    unsigned char* parlcd_mem_base = (unsigned char*)map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if(parlcd_mem_base == NULL){
        fprintf(stderr, "Error - mapping dislay.\n");
        return NULL;
    }
    // Init display
    parlcd_hx8357_init(parlcd_mem_base); 
    
    // Knobs and buttons values
    uint8_t knob1, knob2, knob3, prev1, prev2, prev3, button1, button2, button3;
    read_knobs(&prev1, &prev2, &prev3, &button1, &button2, &button3, knobs_mem_base);
    int buttons_locked = 0;
	int selected_row = 0;
	
	struct timeval t0;
	struct timeval t1;
	gettimeofday(&t0, 0);
    
    while(1) {
    	// Read new knobs and buttons values
        read_knobs(&knob1, &knob2, &knob3, &button1, &button2, &button3, knobs_mem_base);

        // lock button - no change of mode during a small time frame
        buttons_locked--;
        if(buttons_locked < 0) {
        	buttons_locked = 0;
		}
    	
        if(current_display_style == 1) {
			if(knob1 != prev1) {
                if (prev1 > 240 && knob1 < 16) {
                	indexC++;
				} else if (prev1 < 16 && knob1 > 240) {
                	indexC--;
				} else if (prev1 < knob1) {
					indexC++;
				} else indexC--;
				prev1 = knob1;
            }
			if(knob2 != prev2) {
                if (prev2 > 240 && knob2 < 16) {
                	offsetX += ((int)256 - (int)prev2 + (int)knob2)/4;
				} else if (prev2 < 16 && knob2 > 240) {
                	offsetX -= ((int)256 + (int)prev2 - (int)knob2)/4;
				} else if (prev2 < knob2) {
					offsetX += ((int)knob2 - (int)prev2)/4;
				} else offsetX -= ((int)knob2 - (int)prev2)/4;
				prev2 = knob2;
            }
            if(knob3 != prev3) {
                if (prev3 > 240 && knob3 < 16) {
                	offsetY += ((int)256 - (int)prev3 + (int)knob3)/4;
				} else if (prev3 < 16 && knob3 > 240) {
                	offsetY -= ((int)256 + (int)prev3 - (int)knob3)/4;
				} else if (prev3 < knob3) {
					offsetY += ((int)knob3 - (int)prev3)/4;
				} else offsetY -= ((int)knob3 - (int)prev3)/4;
				prev3 = knob3;
            }
            
            if(button2 && !buttons_locked) {
                buttons_locked = 5; // lock buttons
                current_display_style = 2; // set menu
            }
            if(button3 && !buttons_locked) {
                buttons_locked = 5; // lock buttons
                current_display_style = 3; // set menu
				gettimeofday(&t0, 0);
            }
			if (dispArrayAvailable == 1) {
				drawJulia(parlcd_mem_base);
			}
        }
        
        
        else if(current_display_style == 2) {
            if(button1 && !buttons_locked) {
                buttons_locked = 5; // lock buttons
                prev1 = knob1;
                prev2 = knob2;
                prev3 = knob3;
                current_display_style = 1;
                last_x -= 1;
                continue;
            }
            if(button3 && !buttons_locked) {
                buttons_locked = 5; // lock buttons
                prev1 = knob1;
                prev2 = knob2;
                prev3 = knob3;
                current_display_style = 3;
                gettimeofday(&t0, 0);
                continue;
            }
			if(knob2 != prev2) {
                if (prev2 > 240 && knob2 < 16) {
                	selected_row++;
				} else 
				if (prev2 < 16 && knob2 > 240) {
                	selected_row--;
				} else
				if (prev2 < knob2) {
					selected_row++;
				} else selected_row--;
				
				prev2 = knob2;
            }
            
			if(selected_row < 1) selected_row = number_of_rows;
			if(selected_row > number_of_rows) selected_row = 1;
			
			if ((knob3 != prev3) && selected_row == 1) {
                if (prev3 > 240 && knob3 < 16) {
                	if (brightness < 9) brightness++;
				} else 
				if (prev3 < 16 && knob3 > 240) {
                	if (brightness > 0) brightness--;
				} else
				if (prev3 < knob3) {
					if (brightness < 9) brightness++;
				} else if (brightness > 0) brightness--;
				
				prev3 = knob3;
            }
			
			if ((knob3 != prev3) && selected_row == 2){
                if (prev3 > 240 && knob3 < 16) {
                	if (colorMode < maxNumberOfColorMode) colorMode++;
				} else 
				if (prev3 < 16 && knob3 > 240) {
                	if (colorMode > 0) colorMode--;
				} else
				if (prev3 < knob3) {
					if (colorMode < maxNumberOfColorMode) colorMode++;
				} else if (colorMode > 0) colorMode--;
				
				prev3 = knob3;
            }
			if ((knob3 != prev3) && selected_row == 3) {
                if (prev3 > 240 && knob3 < 16) {
                	if (version < maxNumberOfVersion) version++;
				} else 
				if (prev3 < 16 && knob3 > 240) {
                	if (version > 0) version--;
				} else
				if (prev3 < knob3) {
					if (version < maxNumberOfVersion) version++;
				} else if (version > 0) version--;
				
				prev3 = knob3;
            }
			if (dispArrayAvailable == 1) {
				drawMenu(parlcd_mem_base, selected_row);
			}
        }
        
		else if(current_display_style == 3) {
			if (presentation_active == 0) presentation_active = 1;
			gettimeofday(&t1, 0);
			if (timedifference_msec(t0, t1) > 2000) {
				cReRand = (double) (rand() % 10000) / 10000.0;
				cImRand = (double) (rand() % 10000) / 10000.0;
				cRe = cReRand;
				cIm = cImRand;
				offsetX = -240;
				offsetY = -160;
				indexC = 100;
				presentation_active = 2;
				gettimeofday(&t0, 0);
			}
	
            if(button1 && !buttons_locked) {
                buttons_locked = 5; // lock buttons
                prev1 = knob1;
                prev2 = knob2;
                prev3 = knob3;
                current_display_style = 1;
                presentation_active = 0;
                last_x -= 1;
                continue;
            }
            if(button2 && !buttons_locked) {
                buttons_locked = 5; // lock buttons
                prev1 = knob1;
                prev2 = knob2;
                prev3 = knob3;
                current_display_style = 2;
                presentation_active = 0;
                continue;
            }
    		if (dispArrayAvailable == 1) {
			drawJulia(parlcd_mem_base);
			}
        }
	}
	return 0;
}














//	MAIN  //

void startThreads(){
    if(pthread_create(&thread_compute, NULL, Compute, 0)){
        fprintf(stderr, "Error - cannot create Copmute thread\n");
    }
    if(pthread_create(&thread_render, NULL, Rendering, 0)){
        fprintf(stderr, "Error - cannot create Rendering thread\n");
    }
}

void stopThreads(){
    if(pthread_join(thread_compute, NULL)){
        fprintf(stderr, "Error - cannot join Compute thread\n");
    }
    if(pthread_join(thread_render, NULL)){
        fprintf(stderr, "Error - cannot join Render thread\n");
    }
}

int main(int argc, char *argv[])
{
    // Start threads
    startThreads();

    // End application on input
    getchar();
    active = false;
    stopThreads();
    //close(sockfd);

    // exit
    printf("Ending application.\n");
    return 0;
}